import WxPayment from './weixin/payment/index'
import AliPayment from './alipay/payment/index'
import AppleIapPayment from './apple-iap/payment/index'

import { createApi } from './shared/index'

module.exports = {
  initWeixin: (options = {}) => {
    options.clientType = options.clientType || __ctx__.PLATFORM
    return createApi(WxPayment, options)
  },
  initAlipay: (options = {}) => {
    options.clientType = options.clientType || __ctx__.PLATFORM
    return createApi(AliPayment, options)
  },
  initAppleIapPayment: (options = {}) => {
    options.clientType = options.clientType || __ctx__.PLATFORM
    return createApi(AppleIapPayment, options)
  }
}
